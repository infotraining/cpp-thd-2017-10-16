#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <future>
#include "thread_safe_queue.hpp"
#include "raii_thread.hpp"

using namespace std;

using Task = std::function<void()>;

class ThreadPool
{
    const static Task end_of_work;
public:
    ThreadPool(const size_t size) 
    {
        threads_.reserve(size);

        for(size_t i = 0; i < size; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            q_.push(end_of_work);        
    }

    template <typename F>
    auto submit(F&& fun) -> std::future<decltype(fun())>
    {
        using Result = decltype(fun());

        auto task = std::make_shared<std::packaged_task<Result()>>(forward<F>(fun));
        std::future<Result> fresult = task->get_future();
        q_.push([task]() { (*task)(); });

        return fresult;
    }
private:    
    ThreadSafeQueue<Task> q_;    
    vector<Raii::JoiningThread> threads_;

    void run()
    {
        Task task;

        while(true)
        {
            q_.pop(task);

            if (!task)
                return;

            task();    
        }
    }
};

const Task ThreadPool::end_of_work = nullptr;

void background_work(int id, const string& data)
{
    cout << "Starting a THD#" << id << " at thread: " 
        << this_thread::get_id() << endl;

    random_device rd;
    uniform_int_distribution<> distr(100, 2000);

    auto interval = chrono::milliseconds(distr(rd));    

    this_thread::sleep_for(interval);

    cout << "Working on "  << data << endl;

    this_thread::sleep_for(interval);

    cout << "End of a THD#" << id << " at thread: " 
    << this_thread::get_id << endl;
}

string download(const string& url)
{
    return "WEB PAGE";
}

int main()
{    
    ThreadPool thd_pool(4);

    thd_pool.submit([] { cout << "from thread" << endl; });

    for(int i = 0; i < 10; ++i)
        thd_pool.submit([i] { background_work(i, "test_"s + to_string(i)); });

    future<string> content = thd_pool.submit([] { return download("http://google.pl"); });

    cout << content.get() << endl;
}
