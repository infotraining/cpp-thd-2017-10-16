#ifndef RAII_THREAD
#define RAII_THREAD

namespace Raii
{
    class JoiningThread
    {
        std::thread thd_;
    public:
        template <typename Callable, typename... Args, 
            typename = std::enable_if_t<!std::is_same<std::decay_t<Callable>, JoiningThread>::value>
        >
        JoiningThread(Callable&& callable, Args&&... args) 
            : thd_{std::forward<Callable>(callable), std::forward<Args>(args)...}
        {
        }        
        
        JoiningThread(const JoiningThread&) = delete;
        JoiningThread& operator=(const JoiningThread&) = delete;
        
        JoiningThread(JoiningThread&&) = default;
        JoiningThread& operator=(JoiningThread&&) = default;

        std::thread& get()
        {
            return thd_;
        }

        ~JoiningThread()
        {
            if (thd_.joinable())
                thd_.join();
        }
    };

    class DetachedThread
    {
        std::thread thd_;
    public:
        template <typename Callable, typename... Args, 
            typename = std::enable_if_t<!std::is_same<std::decay_t<Callable>, DetachedThread>::value>
        >
        DetachedThread(Callable&& callable, Args&&... args) 
            : thd_{std::forward<Callable>(callable), std::forward<Args>(args)...}
        {
            thd_.detach();
        }        
        
        DetachedThread(const DetachedThread&) = delete;
        DetachedThread& operator=(const DetachedThread&) = delete;
        
        DetachedThread(DetachedThread&&) = default;
        DetachedThread& operator=(DetachedThread&&) = default;                
    };
}

#endif