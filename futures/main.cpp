#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <future>
#include <random>

using namespace std;

string download(const string& url)
{
    cout << "Download of " << url << " started in a thread#" 
        << this_thread::get_id() << endl;

    random_device rd;
    uniform_int_distribution<> distr(100, 2000);

    if (url == "404")
        throw runtime_error("Error#404");

    auto interval = chrono::milliseconds(distr(rd));    

    this_thread::sleep_for(interval);

    cout << "Downloading from "  << url << endl;

    this_thread::sleep_for(interval);    

    return "Content of: "s + url;
}


int main()
{
    vector<future<string>> future_content;

    future<string> f1 = async(launch::async, &download, "http://google.com");
    auto f2 = async(launch::async, [] { return download("http://gigaset.com");});    
    future_content.push_back(async(launch::async, [] { return download("404"); } ));

    while(f1.wait_for(100ms) != future_status::ready)
    {
        cout << "Download in progress..." << endl;
    }

    cout << "\n-----------------------\n";
    
    future_content.push_back(move(f1));
    future_content.push_back(move(f2));
    

    for(auto& f : future_content)
    {
        try
        {
            cout << f.get() << endl;
        }
        catch(const runtime_error& e)
        {
            cout << "Caught: " << e.what() << endl;
        }
    }
}
