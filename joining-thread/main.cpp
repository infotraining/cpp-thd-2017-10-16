#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void background_work(int id, const string& text)
{
    cout << "THD#" << id << " has stared..." << endl;

    for (const auto& c : text)
    {
        cout << c << " ";
        this_thread::sleep_for(100ms); // sleep_for(chrono::milliseconds(100))
        cout.flush();
    }

    cout << "End of THD#" << id << endl;
}

namespace Raii
{
    class JoiningThread
    {
        thread thd_;
    public:
        template <typename Callable, typename... Args, 
            typename = enable_if_t<!is_same<decay_t<Callable>, JoiningThread>::value>
        >
        JoiningThread(Callable&& callable, Args&&... args) 
            : thd_{forward<Callable>(callable), forward<Args>(args)...}
        {
        }        
        
        JoiningThread(const JoiningThread&) = delete;
        JoiningThread& operator=(const JoiningThread&) = delete;
        
        JoiningThread(JoiningThread&&) = default;
        JoiningThread& operator=(JoiningThread&&) = default;

        thread& get()
        {
            return thd_;
        }

        ~JoiningThread()
        {
            if (thd_.joinable())
                thd_.join();
        }
    };

    class DetachedThread
    {
        thread thd_;
    public:
        template <typename Callable, typename... Args, 
            typename = enable_if_t<!is_same<decay_t<Callable>, DetachedThread>::value>
        >
        DetachedThread(Callable&& callable, Args&&... args) 
            : thd_{forward<Callable>(callable), forward<Args>(args)...}
        {
            thd_.detach();
        }        
        
        DetachedThread(const DetachedThread&) = delete;
        DetachedThread& operator=(const DetachedThread&) = delete;
        
        DetachedThread(DetachedThread&&) = default;
        DetachedThread& operator=(DetachedThread&&) = default;                
    };
}

int main()
{
    {
        Raii::JoiningThread thd1{&background_work, 1, "test"};
        Raii::JoiningThread thd2{&background_work, 2, "concurrent"};            
        
        //auto thd3 = thd2;

        vector<Raii::JoiningThread> thds;
        thds.push_back(move(thd1));
        thds.push_back(move(thd2));

        Raii::DetachedThread deamon{&background_work, 665, "deamon deamon deamon"};
    }

    cout << "After scope..." << endl;
} // thd.join();
