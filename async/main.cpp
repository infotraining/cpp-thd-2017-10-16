#include <future>
#include <iostream>

using namespace std;

void background_task(int id)
{
    cout << "Start BT#" << id << endl;
    this_thread::sleep_for(chrono::milliseconds(3000));
    cout << "End BT#" << id << endl;
}


template <typename Callable>
auto spawn_task(Callable&& f)
{
    using result_type = decltype(f());
    packaged_task<result_type ()> pt(std::forward<Callable>(f));
    auto future_result = pt.get_future();

    thread thd{move(pt)};
    thd.detach();

    return future_result;
}

int main()
{
    {
        async(launch::async, background_task, 1);
        async(launch::async, background_task, 2);
    }

    cout << "\n-----------------------" << endl;

    {
        spawn_task( [] { background_task(1); } );
        spawn_task( [] { background_task(2); } );
    }

    cout << "\n-----------------------" << endl;

    {
        auto f1 = async(launch::async, background_task, 1);
        auto f2 = async(launch::async, background_task, 2);
    }
}
