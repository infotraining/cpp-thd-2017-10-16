#include "single_producer_single_consumer_queue.hpp"
#include <thread>

#include <celero/Celero.h>

#include <algorithm>
#include <cassert>
#include <deque>
#include <fstream>
#include <iostream>
#include <list>
#include <random>
#include <vector>

CELERO_MAIN

using namespace std;

constexpr int n = 100'000;
constexpr int no_of_samples = 20;
constexpr int no_of_iterations = 10;

// ------------------------------------------------------------------------------------------------------------
// Tests for ints

struct Tester
{
    int64_t id;
    int value;
};

class QueueFixture : public celero::TestFixture
{
protected:
    std::vector<Tester> data;

    QueueFixture() = default;

    std::vector<std::pair<int64_t, uint64_t>> getExperimentValues() const override
    {
        return {{1000, 0}, {10'000, 0}, {100'000, 0}, {1'000'000, 0}};
    }

    void setUp(int64_t experimentValue) override
    {
        for (int64_t i = 0; i < experimentValue; ++i)
        {
            data.push_back(Tester{i, 0});
        }
    }

    void tearDown() override
    {
        assert(all_of(data.begin(), data.end(), [](const auto& item) { return item.value == 1; }));

        data.clear();
    }
};

BASELINE_F(SpScQueues, with_locks, QueueFixture, no_of_samples, no_of_iterations)
{
    WithLocking::SingleProducerSingleConsumerQueue<Tester*, 100'000> queue;

    auto data_size = data.size();

    thread consumer_thd([&queue, data_size] {
        size_t items_processed = 0;
        while (items_processed < data_size)
        {
            Tester* tester{};
            if (queue.try_deque(tester))
            {
                tester->value = 1;
                ++items_processed;
            }
        }
    });

    // producer
    for (auto& item : data)
    {
        while (!queue.try_enque(&item))
            continue;
    }

    consumer_thd.join();
}

BENCHMARK_F(SpScQueues, lock_free, QueueFixture, no_of_samples, no_of_iterations)
{
    LockFree::SingleProducerSingleConsumerQueue<Tester*, 100'000> queue;

    auto data_size = data.size();

    thread consumer_thd([&queue, data_size] {
        size_t items_processed = 0;
        while (items_processed < data_size)
        {
            Tester* tester{};
            if (queue.try_deque(tester))
            {
                tester->value = 1;
                ++items_processed;
            }
        }
    });

    // producer
    for (auto& item : data)
    {
        while (!queue.try_enque(&item))
            continue;
    }

    consumer_thd.join();
}
