#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void background_work(int id, const string& text)
{
    cout << "THD#" << id << " has stared..." << endl;

    for (const auto& c : text)
    {
        cout << c << " ";
        this_thread::sleep_for(100ms); // sleep_for(chrono::milliseconds(100))
        cout.flush();
    }

    cout << "End of THD#" << id << endl;
}

class BackgroundWork
{
    const int id_{0};

public:
    BackgroundWork() = default;

    BackgroundWork(int id)
        : id_{id}
    {
    }

    void operator()(const string& text) const
    {
        cout << "BW#" << id_ << " has started..." << endl;

        for (const auto& c : text)
        {
            cout << c << " ";
            this_thread::sleep_for(200ms);
        }
        cout.flush();

        cout << "BW#" << id_ << " has finished..." << endl;
    }
};

// dygresja 1 - auto&& vs const auto&
template <typename Container>
void for_each_with_auto(Container&& vec)
{
    for (const auto& x : vec)
    {
        cout << x << endl;
    }

    // syntax sugar for
    for (auto it = vec.begin(); it != vec.end(); ++it)
    {
        auto&& x = *it;

        cout << x << endl;
    }
}

vector<int> create_vec()
{
    return {1, 2, 3};
}



namespace UniversalInitSyntax
{
    struct X
    {
        int a;
        double b;
    
        X(int a, double b)
            : a(a)
            , b(b)
        {
        }
    };

    void Cpp98()
    {
        int x;
        int x1 = 10;
        int x2(10);
        //int x3(); // error

        X val = {1, 3.14}; // error
        X val2(1, 3.14);
        int tab[10] = {1, 2, 3, 4};

        vector<int> vec;
        vec.push_back(1);
        vec.push_back(2);
    }

    double get_value()
    {
        return 3.14;
    }

    void Cpp11()
    {
        int x1;
        double x2{get_value()};
        int x3{};

        X val = {1, 3.14};
        X val2{1, 3.14};

        vector<int> vec = {1, 2, 3, 4};

        vector<int> vec2(10, 1);
        vector<int> vec3{10, 1};
    }
}

static int id = 100;

class Lambda_247365427345
{
public:
    Lambda_247365427345(const string& txt) : text_{txt}
    {}

    void operator()() const
    {
        background_work(id++, text_);
    }
private:
    string text_;
};

// move seamntics for thread
thread create_thread(const string& text)
{
    //thread local_thd{&background_work, id++, text}; // spawn a thread
    //Lambda_247365427345 l{text}
    thread local_thd{[text]{ background_work(id++, text); } };

    return local_thd;
}

int main()
{
    thread thd1{&background_work, 1, "Hello thread..."};
    thread thd2{[] { background_work(2, "Concurrent text..."); }};
    thread thd3{[](int id) { background_work(id, "Concurrent text..."); }, 3};
    thread thd4{BackgroundWork{4}, "Background text..."};

    thread thd5;
    {
        string str = "Another text";
        thd5 = create_thread(str);
    }
    
    double dx = rand() / 1.555;
    cout << dx << endl;    

    vector<thread> thds;

    thds.push_back(move(thd1));
    thds.push_back(move(thd2));
    thds.push_back(move(thd3));
    thds.push_back(move(thd4));
    thds.push_back(move(thd5));
    thds.push_back(create_thread("move semantics"));
    thds.push_back(thread(background_work, 7, "seven"));
    thds.emplace_back(&background_work, 7, "seven");

    for(auto& t : thds)
        t.join();

    const vector<int> data = { 1, 2, 4, 5, 6 };

    vector<int> target1(data.size());
    vector<int> target2(data.size());

    thread cpy_thd1{[&data, &target1] { copy(begin(data), end(data), begin(target1)); }};
    thread cpy_thd2{[&data, &target2] { copy(begin(data), end(data), begin(target2)); }};

    cpy_thd1.join();
    cpy_thd2.join();

    cout << "target1: ";
    for(const auto& item : target1)
        cout << item << " ";
    cout << endl;

    cout << "target2: ";
    for(const auto& item : target2)
        cout << item << " ";
    cout << endl;
}
