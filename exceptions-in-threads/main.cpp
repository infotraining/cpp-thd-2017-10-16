#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void may_throw(int x)
{
    if (x % 2 == 0)
        throw runtime_error("Error#13");
    else
        throw out_of_range("Out of bounds");
}

void background_work(int id, const string& text, exception_ptr&  e)
{
    try
    {
        cout << "THD#" << id << " has stared..." << endl;

        may_throw(id);

        for (const auto& c : text)
        {
            cout << c << " ";
            this_thread::sleep_for(100ms); // sleep_for(chrono::milliseconds(100))
            cout.flush();
        }

        cout << "End of THD#" << id << endl;
    }
    catch (...)
    {
        e = current_exception();

        cout << "THD#" << id << " has safely ended..." << endl;
    }
}

int main() try
{
    const size_t size = 2;

    vector<thread> thds(size);
    vector<exception_ptr> excpts(size);

    for(size_t i = 0; i < size; ++i)
    {
        thds[i] = thread{&background_work, i, "test", ref(excpts[i])};
    }

    for(auto& t : thds)
        t.join();
    
    for(auto& e : excpts)
    {
        if (e)
        {
            rethrow_exception(e);
        }
    }    
}
catch (const runtime_error& e)
{
    cout << "Caught an exception:" << e.what() << endl;
}
catch(const out_of_range& e)
{
    cout << "Handling out of range" << endl;
    cout << "Caught an exception: " << e.what() << endl;
}
