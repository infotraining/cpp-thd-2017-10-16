#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <mutex>
#include <atomic>
#include <condition_variable>

using namespace std;

namespace BusyWait
{
    namespace WithMutex
    {
        class Data
        {
            vector<int> data_;
            bool is_ready_ = false;
            mutex mtx_ready_;
        public:
            void read()
            {
                cout << "Start reading..." << endl;

                data_.resize(100);
                iota(data_.begin(), data_.end(), 1);

                this_thread::sleep_for(1500ms);

                {
                    lock_guard<mutex> lk{mtx_ready_};
                    is_ready_ = true;
                } /////////////////////////////////////////////////// release - mtx_ready_.unlock()
            }

            void process(int id)
            {
                bool ready_flag = false;

                do
                {            
                    lock_guard<mutex> lk{mtx_ready_};  //////////////////////////////////// aquire - mtx_ready_.lock()
                    ready_flag = is_ready_;            
                } while(!ready_flag);
                    

                cout << "Processing data by THD#" << id << "..." << endl;
                auto result = accumulate(data_.begin(), data_.end(), 0);

                cout << "Result: " << result << endl;
            }
        };
    }

    namespace WithAtomics
    {
        class Data
        {
            vector<int> data_;
            atomic<bool> is_ready_{};            
        public:
            void read()
            {
                cout << "Start reading..." << endl;

                this_thread::sleep_for(1500ms);

                data_.resize(100);
                iota(data_.begin(), data_.end(), 1);
                
                is_ready_.store(true, memory_order_release);  /////////////////////////////////// release                               
            }

            void process(int id)
            {                
                while(!is_ready_.load(memory_order_acquire))  /////////////////////////////////// acquire
                    continue;
                    
                cout << "Processing data by THD#" << id << "..." << endl;
                auto result = accumulate(data_.begin(), data_.end(), 0);

                cout << "Result: " << result << endl;
            }
        };
    }
}

namespace WithConditionVariables
{
    class Data
    {
        vector<int> data_;
        bool is_ready_{};            
        mutex mtx_ready_;
        condition_variable cv_ready_;
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(1500ms);

            data_.resize(100);
            iota(data_.begin(), data_.end(), 1);
            
            {
                lock_guard<mutex> lk{mtx_ready_};
                is_ready_ = true;
            }  ///////////////////////// mtx_ready.unlock() - release
            
            cv_ready_.notify_all();
        }

        void process(int id)
        {                
            unique_lock<mutex> lk{mtx_ready_};
            cv_ready_.wait(lk, [this] { return is_ready_; });
            lk.unlock();
                
            cout << "Processing data by THD#" << id << "..." << endl;
            auto result = accumulate(data_.begin(), data_.end(), 0);

            cout << "Result: " << result << endl;
        }
    };
}


int main()
{
    using namespace WithConditionVariables;

    Data data;

    thread thd1{[&data] { data.read(); } };
    thread thd2{[&data] { data.process(1); } };
    thread thd3{[&data] { data.process(2); } };

    thd1.join();
    thd2.join();
    thd3.join();

    cout << "END of main..." << endl;
}
