#ifndef ACTIVE_OBJECT_HPP
#define ACTIVE_OBJECT_HPP

#include "thread_safe_queue.hpp"
#include <functional>
#include <thread>

typedef std::function<void()> Task;

class ActiveObject
{
public:
    ActiveObject() : is_done_(false)
    {
        thread_ = std::thread(&ActiveObject::run, this);
    }

    ActiveObject(const ActiveObject&) = delete;
    ActiveObject& operator=(const ActiveObject&) = delete;

    ~ActiveObject()
    {
        send([&] { is_done_ = true; });

        thread_.join();
    }

    void send(Task task)
    {
        task_queue_.push(task);
    }

private:
    ThreadSafeQueue<Task> task_queue_;
    bool is_done_;
    std::thread thread_;

    void run()
    {
        while (true)
        {
            Task task;

            task_queue_.pop(task);

            task();

            if (is_done_)
                return;
        }
    }
};

#endif // ACTIVE_OBJECT_HPP
