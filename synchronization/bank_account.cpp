#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    const int id_;
    double balance_;
    mutable std::recursive_mutex mtx_;

public:
    BankAccount(int id, double balance) 
        : id_(id), balance_(balance)
    {
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<std::recursive_mutex> lk_from{mtx_, std::defer_lock};
        std::unique_lock<std::recursive_mutex> lk_to{to.mtx_, std::defer_lock };

        std::lock(lk_from, lk_to);

        withdraw(amount);
        deposit(amount);
        //balance_ -= amount;        
        //to.balance_ += amount;
    }

    void withdraw(double amount)
    {
        std::lock_guard<std::recursive_mutex> lk{mtx_};
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<std::recursive_mutex> lk{mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<std::recursive_mutex> lk{mtx_};
        return balance_;
    }
};

namespace Proxy
{
    class BankAccount
    {
        const int id_;
        double balance_;
    
    public:
        BankAccount(int id, double balance) 
            : id_(id), balance_(balance)
        {
        }
    
        void print() const
        {
            std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
        }
    
        void transfer(BankAccount& to, double amount)
        {
            balance_ -= amount;
            to.balance_ += amount;
        }
    
        void withdraw(double amount)
        {            
            balance_ -= amount;
        }
    
        void deposit(double amount)
        {            
            balance_ += amount;
        }
    
        int id() const
        {
            return id_;
        }
    
        double balance() const
        {            
            return balance_;
        }
    };    

    class ProxyBankAccount
    {
        BankAccount account_;
        mutable std::mutex mtx_;
    
    public:
        ProxyBankAccount(int id, double balance) 
            : account_(id, balance)
        {
        }
    
        void print() const
        {
            std::lock_guard<std::mutex> lk{mtx_};
            account_.print();
        }
    
        void transfer(BankAccount& to, double amount)
        {
            account_.transfer(to, amount);            
        }
    
        void withdraw(double amount)
        {            
            std::lock_guard<std::mutex> lk{mtx_};
            account_.withdraw(amount);
        }
    
        void deposit(double amount)
        {            
            std::lock_guard<std::mutex> lk{mtx_};
            account_.deposit(amount);
        }


        int id() const
        {
            std::lock_guard<std::mutex> lk{mtx_};
            return account_.id();
        }
    
        double balance() const
        {      
            std::lock_guard<std::mutex> lk{mtx_};
            return account_.balance();
        }
    };
}


namespace PolicyBasedDesign
{
    struct SingleThreaded
    {
        struct EmptyLockable
        {
            void lock() {}
            void unlock() {}
        };
    
        using Mutex = EmptyLockable;
    protected:
        mutable Mutex mtx_;
    };
    

    struct MultiThreaded
    {
        using Mutex = std::mutex;
    protected:
        mutable Mutex mtx_;
    };
    

    template <typename ThreadingPolicy>
    class BankAccount : protected ThreadingPolicy
    {   
        const int id_;
        double balance_;
        using mutex_type = typename ThreadingPolicy::Mutex;
    public:
        BankAccount(int id, double balance) 
        : id_(id), balance_(balance)
        {
        }

        void print() const
        {
            std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
        }

        void transfer(BankAccount& to, double amount)
        {
            std::lock(ThreadingPolicy::mtx_, to.mtx_);
            std::lock_guard<mutex_type> lk_from{ThreadingPolicy::mtx_, std::adopt_lock};
            std::lock_guard<mutex_type> lk_to{to.ThreadingPolicy::mtx_, std::adopt_lock};

            balance_ -= amount;
            to.balance_ += amount;
        }

        void withdraw(double amount)
        {
            std::lock_guard<mutex_type> lk{ThreadingPolicy::mtx_};
            balance_ -= amount;
        }

        void deposit(double amount)
        {            
            std::lock_guard<mutex_type> lk{ThreadingPolicy::mtx_};
            balance_ += amount;
        }

        int id() const
        {            
            return id_;
        }
    
        double balance() const
        {      
            std::lock_guard<mutex_type> lk{ThreadingPolicy::mtx_};
            return balance_;
        }
    };
}

template <typename AccountType>
void make_withdraws(AccountType& ba, int count, double amount)
{
    for(int i = 0; i < count; ++i)
        ba.withdraw(amount);
}

template <typename AccountType>
void make_deposits(AccountType& ba, int count, double amount)
{
    for(int i = 0; i < count; ++i)
        ba.deposit(amount);
}

template <typename AccountType>
void make_transfers(AccountType& from, AccountType& to, int count, double amount)
{
    for(int i = 0; i < count; ++i)
        from.transfer(to, amount);
}

int main()
{
    std::cout << "Before:\n";
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    std::thread thd1{&make_deposits<BankAccount>, std::ref(ba1), 1'000'000, 1.0 };
    std::thread thd2{&make_withdraws<BankAccount>, std::ref(ba1), 1'000'000, 1.0 };

    std::thread thd3{&make_transfers<BankAccount>, std::ref(ba1), std::ref(ba2), 1'000'000, 1.0 };
    std::thread thd4{&make_transfers<BankAccount>, std::ref(ba2), std::ref(ba1), 1'000'000, 1.0 };

    thd1.join();
    thd2.join();
    thd3.join();
    thd4.join();

    std::cout << "\nAfter:\n";
    ba1.print();
    ba2.print();

    std::cout << "\n\n";
    std::cout << "PBD - before:\n";

    using MyAccount = PolicyBasedDesign::BankAccount<PolicyBasedDesign::MultiThreaded>;

    MyAccount ac1(3, 2'000);
    MyAccount ac2(4, 2'000);

    ac1.print();
    ac2.print();

    std::thread thd5{ &make_deposits<MyAccount>, std::ref(ac1), 1'000'000, 1.0 };
    std::thread thd6{ [&ac1] { make_withdraws(ac1, 1'000'000, 1.0); } };

    std::thread thd7{&make_transfers<MyAccount>, std::ref(ac1), std::ref(ac2), 1'000'000, 1.0 };
    std::thread thd8{&make_transfers<MyAccount>, std::ref(ac2), std::ref(ac1), 1'000'000, 1.0 };

    thd5.join();
    thd6.join();
    thd7.join();
    thd8.join();

    std::cout << "\nPBD - after:\n";

    ac1.print();
    ac2.print();
    
    ba1.deposit(40); 
    {                    
        std::lock_guard<BankAccount> lk{ba1}; // begin transaction                       
        ba1.deposit(100.0);            
        ba1.withdraw(25.0);        
    } // commit transaction    
}
