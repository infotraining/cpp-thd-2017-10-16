#include <atomic>
#include <boost/intrusive_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <iostream>
#include <thread>
#include <mutex>

// definicja funkcji intrusive_ptr_add_ref i intrusive_ptr_release
template <typename T>
void intrusive_ptr_add_ref(T* t)
{
    t->add_ref();
}

template <typename T>
void intrusive_ptr_release(T* t)
{
    t->release();
}

namespace ThreadUnsafe
{
    // klasa licznika
    template <typename T>
    class RefCounted : boost::noncopyable
    {
        int ref_count_{0};

    public:
        RefCounted() = default;

        void add_ref()
        {
            ++ref_count_;
        }

        void release()
        {
            if (--ref_count_ == 0)
            {
                delete static_cast<T*>(this);
            }
        }
    };
}

namespace ThreadSafe
{
    // klasa licznika
    template <typename T>
    class RefCounted : boost::noncopyable
    {
        std::atomic<int> ref_count_{0};

    public:
        RefCounted() = default;

        void add_ref()
        {
            ref_count_.fetch_add(1, std::memory_order_relaxed);
        }

        void release()
        {
            if (ref_count_.fetch_sub(1, std::memory_order_acq_rel) == 1)
            {
                delete static_cast<T*>(this);
            }
        }
    };
}

// zarzadzana klasa
class Gadget : public ThreadUnsafe::RefCounted<Gadget>
{
public:
    Gadget()
    {
        std::cout << "Gadget::Gadget()\n";
    }

    Gadget(const Gadget&) = delete;
    Gadget& operator=(const Gadget&) = delete;

    ~Gadget()
    {
        std::cout << "Gadget::~Gadget()\n";
    }
};

int main()
{
    using namespace std;

    thread thd1, thd2;
    {
        boost::intrusive_ptr<Gadget> p1(new Gadget());  // rc = 1
        {
            boost::intrusive_ptr<Gadget> p2 = p1; // rc = 2

            thd1 = std::thread([p2] { std::this_thread::sleep_for(1000ms); }); // rc = 3
        } // rc = 2

        thd2 = std::thread([p1] { std::this_thread::sleep_for(1000ms); }); // rc = 3
    } // rc = 2

    thd1.join(); // rc = 1
    thd2.join(); // rc = 0 - delete Gadget
}


void smart_ptrs_with_dealloc()
{
    std::unique_ptr<FILE, int(*)(FILE*)> my_ufile(fopen("text.txt", "+r"), &fclose);

    fprintf(my_ufile.get(), "test");

    std::shared_ptr<FILE> my_sfile(fopen("text.txt"), [](FILE* f) { if (f) fclose(f); });

    fprintf(my_sfile.get(), "test");

    //may_throw()    
}

namespace Singletons
{
    namespace Leaky
    {
        class Singleton
        {
            static std::atomic<Singleton*> instance_;
            static std::mutex mtx_;
        
            Singleton() {}
        
        public:
            Singleton(const Singleton&) = delete;
            Singleton& operator=(const Singleton&) = delete;
        
            static Singleton& instance()
            {
                if (instance_ == nullptr) // load(mo::sq)
                {
                    std::lock_guard<std::mutex> lk{mtx_};
        
                    if (instance_ == nullptr)
                    {
                        //instance_ = new Singleton();
                        void* raw_mem = ::operator new(sizeof(Singleton));                        
                        new (raw_mem) Singleton();
                        instance_.store(static_cast<Singleton*>(raw_mem));  // store(mo::sq)
                        
                    }
                }
        
                return *instance_;
            }
        };
    }

    namespace Meyers
    {
        class Singleton
        {            
            Singleton() {}
        
        public:
            Singleton(const Singleton&) = delete;
            Singleton& operator=(const Singleton&) = delete;
        
            static Singleton& instance()
            {
                static Singleton unique_instance;
        
                return unique_instance;
            }
        };
    }

    namespace LazyLoading
    {
        class Singleton
        {
            static std::unique_ptr<Singleton> instance_;            
            static std::once_flag init_flag_;
        
            Singleton() {}
        
        public:
            Singleton(const Singleton&) = delete;
            Singleton& operator=(const Singleton&) = delete;
        
            static Singleton& instance()
            {
                std::call_once(init_flag_, [] { instance_ = std::make_unique<Singleton>(); });
        
                return *instance_;
            }
        };
    }
}
