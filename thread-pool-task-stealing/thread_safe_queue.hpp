#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <condition_variable>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>

template <typename T>
class ThreadSafeQueue
{
    std::queue<T> queue_;
    bool is_done_{false};
    std::mutex mtx_;
    std::condition_variable cv_item_pushed_;

public:
    ThreadSafeQueue() = default;

    ThreadSafeQueue(const ThreadSafeQueue&) = delete;
    ThreadSafeQueue& operator=(const ThreadSafeQueue&) = delete;

    ThreadSafeQueue(ThreadSafeQueue&&) = delete;
    ThreadSafeQueue& operator=(ThreadSafeQueue&&) = delete;

    void done()
    {
        {
            std::unique_lock<std::mutex> lk{mtx_};
            is_done_ = true;
        }

        cv_item_pushed_.notify_all();
    }

    void push(const T& item)
    {
        {
            std::lock_guard<std::mutex> lk(mtx_);
            queue_.push(item);
        }
        cv_item_pushed_.notify_one();
    }

    void push(T&& item)
    {
        {
            std::lock_guard<std::mutex> lk(mtx_);
            if (is_done_)
                throw std::runtime_error("pushing after done is not allowed");

            queue_.push(std::move(item));
        }
        cv_item_pushed_.notify_one();
    }

    bool pop(T& item)
    {
        std::unique_lock<std::mutex> lk(mtx_);

        while (queue_.empty() && !is_done_)
            cv_item_pushed_.wait(lk);

        if (queue_.empty() && is_done_)
            return false;

        item = std::move(queue_.front());
        queue_.pop();
        return true;
    }

    bool try_pop(T& item)
    {
        std::unique_lock<std::mutex> lk(mtx_, std::try_to_lock);
        if (!lk || queue_.empty())
            return false;

        item = std::move(queue_.front());
        queue_.pop();

        return true;
    }

    bool try_push(const T& item)
    {
        {
            std::unique_lock<std::mutex> lk(mtx_, std::try_to_lock);
            
            if (!lk)
                return false;

            if (is_done_)
                throw std::runtime_error("pushing after done is not allowed");

            queue_.push(item);
        }
        cv_item_pushed_.notify_one();

        return true;
    }

    /*bool try_push(T&& item)
    {
        {
            std::unique_lock<std::mutex> lk(mtx_, std::try_to_lock);
            
            if (!lk)
                return false;

            if (is_done_)
                throw std::runtime_error("pushing after done is not allowed");

            queue_.push(std::move(item));
        }
        cv_item_pushed_.notify_one();

        return true;
    }*/
};

#endif // THREAD_SAFE_QUEUE_HPP
