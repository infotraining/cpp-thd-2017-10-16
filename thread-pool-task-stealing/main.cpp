#define CATCH_CONFIG_MAIN

#include "catch/catch.hpp"

#include "thread_safe_queue.hpp"
#include <algorithm>
#include <atomic>
#include <chrono>
#include <functional>
#include <iostream>
#include <random>
#include <thread>
#include <vector>

using namespace std;

typedef std::function<void()> Task;

namespace TaskStealing
{
    class ThreadPool
    {
    public:
        ThreadPool(size_t no_of_threads
                   = std::max(1u, std::thread::hardware_concurrency()))
            : no_of_threads_{no_of_threads}
            , task_queues_(no_of_threads)
            , threads_(no_of_threads)
            , index_{0}
        {
            for (size_t i = 0; i < no_of_threads_; ++i)
                threads_[i] = std::thread{[this, i] { run(i); }};
        }

        ThreadPool(const ThreadPool&) = delete;
        ThreadPool& operator=(const ThreadPool&) = delete;

        ~ThreadPool()
        {
            for (auto& tq : task_queues_)
                tq.done();

            for (auto& t : threads_)
                t.join();        
        }

        template <typename F>
        void submit(F&& fun)
        {
            const auto i = index_++;

            for (size_t n = 1; n < no_of_threads_ * stealing_factor; ++n)
            {
                if (task_queues_.at((i + n) % no_of_threads_)
                        .try_push(fun))
                    return;
            }

            task_queues_[i % no_of_threads_].push(std::forward<F>(fun));
        }

    private:
        static const unsigned int stealing_factor = 24;

        const size_t no_of_threads_;
        std::vector<ThreadSafeQueue<Task>> task_queues_;
        std::vector<std::thread> threads_;
        std::atomic<unsigned int> index_;

        void run(unsigned int i)
        {
            while (true)
            {
                Task task;
                bool item_stealed = false;

                for (size_t n = 1; n < no_of_threads_; ++n)
                {
                    if (task_queues_.at((i + n) % no_of_threads_).try_pop(task))
                    {
                        item_stealed = true;                        
                        break;
                    }
                }

                if (!item_stealed)
                {
                    if (!task_queues_[i].pop(task))
                        break;                    
                }

                task();
            }
        }
    };
}

void background_task(int id)
{
    // cout << "BT#" << id << " starts..." << endl;
    
    mt19937_64 rnd(id);
    uniform_int_distribution<> distr(1, 1000);

    vector<int> data(distr(rnd));
    generate(data.begin(), data.end(), [&] { return distr(rnd); });
    sort(data.begin(), data.end());

    // cout << "BT#" << id << " ends..." << endl;
}


TEST_CASE("task stealing")
{
    const size_t n = 16000;

    using Data = vector<int>;

    vector<Data> data(n);

    random_device rd;
    mt19937_64 rnd_engine(rd());
    uniform_int_distribution<> rnd_gen(1, 16000);

    for (size_t i = 0; i < n; ++i)
    {
        const size_t size = 4;
        data[i].resize(size);
        generate(data[i].begin(), data[i].end(), [&] { return rnd_gen(rnd_engine); });
    }

    {
        TaskStealing::ThreadPool pool;

        for (size_t i = 0; i < n; ++i)
        {
            pool.submit([i, &data] { sort(data[i].begin(), data[i].end()); });
        }
    }

    auto result = all_of(data.begin(), data.end(), [](const auto& items) { return is_sorted(items.begin(), items.end()); });
    REQUIRE(result);
}